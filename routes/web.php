<?php

use Illuminate\Support\Facades\Route;

//ACESSO
Route::get('/',                     'AcessoController@login');
Route::get('/Login',                'AcessoController@login');
Route::post('/Login',               'AcessoController@verifica');
Route::get('/Cadastro',             'AcessoController@registro');
Route::post('/Cadastro',            'AcessoController@registroPost');
Route::post('/RedefinirSenha',      'AcessoController@forgotPost');
Route::get('/Recuperacao/{token}',  'AcessoController@recuperacao');
Route::post('/Recuperacao/{token}', 'AcessoController@recuperacaoPost');
Route::get('/Logout',               'AcessoController@logout');

//DASHBOARD
Route::get('/Dashboard',    'DashboardController@dashboard');
