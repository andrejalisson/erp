<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id_log');
            $table->string('titulo_log', 50);
            $table->longText('descricao_log')->nullable();
            $table->integer('usuarioId_log')->nullable();
            $table->dateTime('criacao_log')->nullable();
            $table->boolean('status_log')->default(true);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('logs');
    }
}
