<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('notificacoes', function (Blueprint $table) {
            $table->increments('id_not');
            $table->string('titulo_not', 50)->nullable();
            $table->string('descricao_not', 100)->nullable();
            $table->integer('usuarioId_not')->nullable();
            $table->dateTime('criacao_not')->nullable();
            $table->boolean('status_not')->default(true);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacoes');
    }
}
