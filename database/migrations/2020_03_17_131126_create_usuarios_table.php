<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id_user');
            $table->string('nome_user', 100);
            $table->string('senha_user', 72)->nullable();
            $table->string('email_user', 100)->nullable();
            $table->string('CPF_user', 11)->nullable();
            $table->string('usarname_user', 20)->nullable();
            $table->date('criacao_user')->nullable();
            $table->date('alteracao_user')->nullable();
            $table->boolean('status_user')->default(true);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
