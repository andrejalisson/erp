<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecuperaSenhaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('RecuperaSenha', function (Blueprint $table) {
            $table->increments('id_rsenh');
            $table->string('token_rsenh', 15);
            $table->integer('usuarioID_rsenh');
            $table->date('criacao_rsenh')->nullable();
            $table->boolean('status_rsenh')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RecuperaSenha');
    }
}
