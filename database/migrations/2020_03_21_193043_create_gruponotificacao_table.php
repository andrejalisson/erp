<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGruponotificacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gruponotificacao', function (Blueprint $table) {
            $table->increments('id_Gnot');
            $table->string('titulo_Gnot', 50)->nullable();
            $table->dateTime('criacao_Gnot')->nullable();
            $table->boolean('status_Gnot')->default(true);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gruponotificacao');
    }
}
