@extends('template.acesso')

@section('css')

@endsection

@section('corpo')
<section id="wrapper" class="login-register login-sidebar" style="background-image:url(/assets/images/background/login-register.jpg);">
    <div class="login-box card">
        <div class="card-body">
            <form class="form-horizontal form-material" id="loginform" method="POST" action="/Cadastro">
                {!! csrf_field() !!}
                <div class="text-center">
                    <a href="javascript:void(0)" class="db"><img src="/assets/images/logo-icon.png" alt="Home" /><br/><img src="/assets/images/logo-text.png" alt="Home" /></a>
                </div>
                <h3 class="box-title m-t-40 m-b-0">Cadastre-se</h3><small>Seu cadastro será avaliado pelo nosso CPD</small>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" name="nome" placeholder="Nome">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" required="" name="email" placeholder="E-mail">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" name="usuario" placeholder="Usuário">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" name="senha" placeholder="Senha">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Cadastrar</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Já é cadastrado? <a href="/" class="text-info m-l-5"><b>Efetue o Login</b></a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection

@section('js')

@endsection

@section('script')
    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
    </script>
@endsection