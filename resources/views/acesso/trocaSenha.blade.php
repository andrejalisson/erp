@extends('template.acesso')

@section('css')

@endsection

@section('corpo')
<section id="wrapper" class="login-register login-sidebar" style="background-image:url(/assets/images/background/login-register.jpg);">
    <div class="login-box card">
        <div class="card-body">
            <form class="form-horizontal form-material text-center" id="loginform" method="POST" action="/Recuperacao/{{$token}}">
                {!! csrf_field() !!}
                <a href="javascript:void(0)" class="db"><img src="/assets/images/logo-icon.png" alt="Home" /><br/><img src="/assets/images/logo-text.png" alt="Home" /></a>
                <div class="form-group m-t-40">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="senha" placeholder="Nova Senha">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection

@section('js')

@endsection

@section('script')
<script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
</script>

<script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '2535933660006399',
        cookie     : true,
        xfbml      : true,
        version    : 'v6.0'
      });
        
      FB.AppEvents.logPageView();   
        
    };
  
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>
@endsection