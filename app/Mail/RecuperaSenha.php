<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecuperaSenha extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $email;
    public $token;

    public function __construct($nome, $email, $token){
        $this->nome     = $nome;
        $this->email    = $email;
        $this->token    = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->from('contato@lotsisten.com.br')
                    ->subject('Recuperar Senha LotSisten.')
                    ->view('emails.recuperaSenha')
                    ->with('nome', 'token');
    }
}
