<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\CadastroInicial;
use App\Mail\RecuperaSenha;

class AcessoController extends Controller{

    public function login(){
        $title = "Login";
        return view('acesso.login')->with(compact('title'));
    }

    public function registro(){
        $title = "Login";
        return view('acesso.cadastro')->with(compact('title'));
    }

    public function registroPost(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->email)->orwhere('usarname_user', $request->usuario)->first();
        if($usuario != null){
            if ($usuario->status_user == 0) {
                $request->session()->flash('alerta', 'Seu cadastro ainda não passou pela análise do nosso CPD, favor aguardar.');
                return redirect()->back();
            }
        }else{
            DB::table('usuarios')->insert([
                'nome_user'         => $request->nome,
                'senha_user'        => password_hash($request->senha, PASSWORD_DEFAULT),
                'email_user'        => $request->email,
                'usarname_user'     => $request->usuario,
                'criacao_user'      => date("Y-m-d"),
                'alteracao_user'    => date("Y-m-d"),
                'status_user'       => 0,
                ]
            );
            \Mail::to($request->email)->send(new CadastroInicial());
            $request->session()->flash('sucesso', 'Um email foi enviado com as instruções! Aguarde a análise do nosso CPD.');
            return redirect('/');
        }
    }

    public function verifica(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->usuario)->orwhere('usarname_user', $request->usuario)->where('status_user', 1)->first();
        if($usuario != null){
            if(password_verify($request->senha, $usuario->senha_user )){
                $request->session()->flash('sucesso', 'Bom trabalho!');
                $request->session()->put('id', $usuario->id_user);
                $request->session()->put('nome', $usuario->nome_user);
                $request->session()->put('login', $usuario->usarname_user);
                $request->session()->put('logado', true);
                return redirect('/Dashboard');
            }else{
                $request->session()->flash('alerta', 'Senha incorreta!');
                return redirect()->back();
            }
        }else{
            $request->session()->flash('alerta', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function forgotPost(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->usuario)->orwhere('usarname_user', $request->usuario)->where('status_user', 1)->first();
        if($usuario != null){
            $token = uniqid('Aa');
            DB::table('RecuperaSenha')->insert([
                'token_rsenh'       => $token,
                'usuarioID_rsenh'   => $usuario->id_user,
                'criacao_rsenh'     => date("Y-m-d"),
                'status_rsenh'      => true,
                ]
            );
            \Mail::to($usuario->email_user)->send(new RecuperaSenha($usuario->nome_user, $usuario->email_user, $token));
            $request->session()->flash('sucesso', 'Um email foi enviado com as instruções!');
            return redirect('/');
        }else{
            $request->session()->flash('alerta', 'Usuário ou E-mail não encontrado.');
            return redirect()->back();
        }
    }

    public function recuperacao(Request $request, $token){
        $title = "Redefinir Senha";
        $usuario = DB::table('RecuperaSenha')->where('token_rsenh', $token)->where('status_rsenh', 1)->first();
        if ($usuario != null) {
            return view('acesso.trocaSenha')->with(compact('token', 'title'));
        }else{
            $request->session()->flash('alerta', 'Token inválido, favor repetir o processo de recuperação.');
            return redirect('/');
        }
        
    }

    public function recuperacaoPost(Request $request, $token){
        $recupera = DB::table('RecuperaSenha')->where('token_rsenh', $token)->where('status_rsenh', 1)->first();
        DB::table('RecuperaSenha')
            ->where('token_rsenh', $token)
            ->update(['status_rsenh' => 0]
        );
        DB::table('usuarios')
            ->where('id_user', $recupera->usuarioID_rsenh)
            ->update(['senha_user'=> password_hash($request->senha, PASSWORD_DEFAULT)]
        );
        $request->session()->flash('sucesso', 'Senha redefinida com sucesso.');
        return redirect('/Dashboard');
    }

    public function logout(){
        session()->flush();
        $request->session()->flash('sucesso', 'Até Logo!');
        return redirect('/Login');
    }
}
